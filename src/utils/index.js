export const addUserData = (value) => {
  localStorage.setItem("user", JSON.stringify(value.user)); // Save userData
  localStorage.setItem("accessToken", value.accessToken); // Save access Token
  localStorage.setItem("authenticated", value.isAuthenticated); // Save authentication status
};

export const deleteUserData = () => {
  localStorage.removeItem("user"); // delete userData
  localStorage.removeItem("accessToken"); // delete access Token
  localStorage.removeItem("authenticated"); // delete authentication status
};

const getDataFromLocalStorage = (key) => {
  return localStorage.getItem(key);
};

export const getInitialState = () => {
  const user = getDataFromLocalStorage("user");
  const accessToken = getDataFromLocalStorage("accessToken");
  const authStatus = getDataFromLocalStorage("authenticated");

  const initialState = {
    user: user ? JSON.parse(user) : null,
    isAuthenticated: authStatus ? authStatus : false,
    accessToken: accessToken ? accessToken : null,
  };
  return initialState;
};
