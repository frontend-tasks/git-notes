import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { loginSuccess } from "../../redux/reducer";
import {
  loginWithGithub,
  getAccessToken,
  fetchUserData,
} from "../../utils/login";
import "./index.scss";

const LoginButton = () => {
  const [user, setUser] = useState(null);
  const [accessToken, setAccessToken] = useState(null);
  const dispatch = useDispatch();

  const handleLogin = () => {
    loginWithGithub();
  };

  useEffect(() => {
    const urlParams = new URLSearchParams(window.location.search);
    const codeParams = urlParams.get("code");
    if (codeParams) {
      getAccessToken(setAccessToken, codeParams);
    }
  }, []);

  useEffect(() => {
    if (accessToken) {
      fetchUserData(setUser, accessToken);
    }
  }, [accessToken]);

  useEffect(() => {
    if (user) {
      dispatch(loginSuccess({ user, accessToken }));
    }
  }, [user, accessToken, dispatch]);

  return (
    <button className="loginBtn" onClick={handleLogin}>
      Login
    </button>
  );
};

export default LoginButton;
