import { createSlice } from "@reduxjs/toolkit";
import { getInitialState, addUserData, deleteUserData } from "../utils";

const gitNotesSlice = createSlice({
  name: "gists",
  initialState: getInitialState(),
  reducers: {
    loginSuccess: (state, action) => {
      state.user = action.payload.user;
      state.accessToken = action.payload.accessToken;
      state.isAuthenticated = true;
      addUserData(state);
    },
    logout: (state) => {
      state.user = null;
      state.accessToken = null;
      state.isAuthenticated = false;
      deleteUserData();
    },
  },
});

export const { loginSuccess, logout } = gitNotesSlice.actions;
export default gitNotesSlice.reducer;
