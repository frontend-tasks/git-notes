const GITHUB_ACCESS_TOKEN_URL = "https://github.com/login/oauth/access_token";
const GITHUB_USER_DATA_URL = "https://api.github.com/user";

module.exports = {
  GITHUB_ACCESS_TOKEN_URL,
  GITHUB_USER_DATA_URL,
};
